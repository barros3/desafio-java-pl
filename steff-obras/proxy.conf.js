const proxy = [
  {
    context: '/mood',
    target: 'http://localhost:8080/mood',
    pathRewrite: { '^/mood': '' }
  }
];

module.exports = proxy;
