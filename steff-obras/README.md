README.tzt

@author Luciano Ferreira

#Material utilizado

1 - openjdk 13 2019-09-17
2 - OpenJDK Runtime Environment (build 13+33)
3 - OpenJDK 64-Bit Server VM (build 13+33, mixed mode, sharing)
4 - @angular-devkit/architect    0.803.5
5 - @angular-devkit/core         8.3.5
6 - @angular-devkit/schematics   8.3.5
7 - @schematics/angular          8.3.5
8 - @schematics/update           0.803.5
9 - rxjs                         6.4.0

#Passos para executar a aplicação:

1 - crie uma data base no phpmyadmin chamada `goku`, e rode na porta 80;
2 - importe no eclipse STS o projeto backend `mood`, e rode na porta 8080;
3 - importe no visual basic o projeto frontend `steff-obras`;
3.1 - abra na raiz do projeto e rode `npm install`;
3.2 - ainda na raiz rode `npm start`;
3.3 - acesse no browser localhost:4200;

#Uso

Ao rodar a aplicação, ela exibirá um menu contendo 4 modulos:

1 - Add Autor;
1.1 - Tela de cadastro do autor;
2 - List Autor;
2.1 - Tela onde se realiza pesquisa dos autores;
2.2 - Ao listar os autores o usuario terá 4 ações;
2.2.1 - Vicular Obra (Em andamento);
2.2.2 - Editar Autor;
2.2.3 - Remover Autor;
2.2.4 - Detalhar Autor;
2.3 - Lista autores para vincular a obra(Em andamento);
3 - Add Obra;
3.1 - Tela de Cadastro da Obra (em andamento);
3.1.1 - Lista/Seleciona autores para vincular a obra;
3.2 - Lista Obras(em andamento)

#Em andamento

=> Concluir o cadastro da obra vinculando a imagem ao google drive;
=> Concluir a autenticaão oauth2;
=> Listar a Obras;
=> Implementar ações CRUD/Vincular na lista de Obras;
=> Listar Obras no cadastro de autor, realizar vinculo;
=> Implementar ação vínculo na lista de autores;

# SteffObras

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# steff-obras