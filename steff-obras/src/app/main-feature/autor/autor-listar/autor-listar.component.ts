import { Component, OnInit } from '@angular/core';
import { AutorService } from '../../../service/autor-service.service';
import { Autor } from '../../../model/autor';
import { Observable } from 'rxjs';
import { ModalService } from '../../../shared/_modal';

@Component({
  selector: 'app-autor-listar',
  templateUrl: './autor-listar.component.html',
  styleUrls: ['./autor-listar.component.css']
})
export class AutorListarComponent implements OnInit {
  autores: Observable<Autor[]>;

  public paginaAtual = 1;
  public total;

  public showAutorAtualizado = false;
  public byAtualizar = true;

  autor: Autor = new Autor();

  isEdit = true;
  titleDetail: string = "Detalhar";
  titleSave: string = "Editar";

  constructor(private autorService: AutorService, private modalService: ModalService) { }

  ngOnInit() {

  }

  reloadData() {
    this.autorService.getAllAutores().subscribe(
      data => {
        this.autores = data.resposta;
        this.total = data.contador;
      },
      error => console.log("Erro ao carregar os Autores: " + error),
      () => console.log('Feito.')
    );
  }

  removeAutor(id: number) {
    this.autorService.deleteAutor(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  pesquisar() {

    if (this.autor.cpf == null || this.autor.cpf === undefined) {
      this.reloadData();
      return;
    }

    this.autorService.findAutorByCpf(this.autor)
      .subscribe(
        data => {
          this.autores = data.resposta;
          this.total = data.contador;
          console.log(data);
        },
        error => {
          this.limpar();
          console.log(error);
        });
  }

  onSubmit() {
    this.pesquisar();
  }

  limpar() {
    this.total = -1;
    this.autor.cpf = undefined;
  }

  detalharAutor(obj: any, id: any) {
    this.autor = obj;
    this.modalService.open(id);
  }

  fecharModal(id: string) {
    this.showAutorAtualizado = false;
    this.byAtualizar = true;
    this.modalService.close(id);
  }

  vincularObra(autor: any) {

  }

  showModalAtualizarAutor(obj: any, id: any) {
    this.byAtualizar = false;
    this.autor = obj;
    this.modalService.open(id);
  }

  atualizarAutor() {
    this.autorService.updateAutor(this.autor.id, this.autor)
      .subscribe(data => {
        console.log(data);
        this.showAutorAtualizado = true;
        this.byAtualizar = true;
      },
        error => console.log(error));
    this.autor = new Autor();
  }

}

