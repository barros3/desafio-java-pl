import { Component, OnInit } from '@angular/core';
import { AutorService } from '../../service/autor-service.service';
import { Autor } from '../../model/autor';

@Component({
  selector: 'app-autor',
  templateUrl: './autor.component.html',
  styleUrls: ['./autor.component.css']
})
export class AutorComponent implements OnInit {

  autor: Autor = new Autor();
  showAutorCadastrado = false

  autorService: AutorService;

  constructor(autorService: AutorService) {
    this.autorService = autorService;
  }

  ngOnInit() { }

  onSubmit() {
    this.save();
  }

  save() {
    this.autorService.addAutor(this.autor)
      .subscribe(data => {
        console.log(data);
        this.showAutorCadastrado = true;
      },
        error => console.log(error));
    this.autor = new Autor();
  }

}
