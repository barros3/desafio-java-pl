import { Component, OnInit } from '@angular/core';
import { Autor } from '../../model/autor';
import { Obra } from '../../model/Obra';
import { ObraService } from '../../service/obra-service.service';
import { AutorService } from '../../service/autor-service.service';

@Component({
  selector: 'app-obra',
  templateUrl: './obra.component.html',
  styleUrls: ['./obra.component.css']
})
export class ObraComponent implements OnInit {

  public showObraCadastrada = false;
  obra: Obra = new Obra();

  public paginaAtual = 1;
  public paginaAtualVinculados = 1;
  public total;
  public totalAutoresVinculados;

  public showMensagem = false;

  autores: Array<Autor> = [];
  autoresParaVicular: Array<Autor> = [];

  constructor(private obraService: ObraService, private autorService: AutorService) { }

  ngOnInit() {
    this.reloadData();
  }

  onSubmit() {
    this.save();
  }

  save() {
    this.obra.autor = this.autoresParaVicular;
    this.obraService.addObra(this.obra)
      .subscribe(data => {
        console.log(data);
        this.showObraCadastrada = true;
      },
        error => console.log(error));
    this.obra = new Obra();
    this.autoresParaVicular = [];
  }

  adicionarAutor(autor: Autor) {

    let id = this.autoresParaVicular.indexOf(autor);

    if (id === -1) {
      this.showMensagem = false;
      this.autoresParaVicular.push(autor);
      let indexRemove = this.autores.indexOf(autor);
      this.autores.splice(indexRemove, 1);
      this.totalAutoresVinculados = this.autoresParaVicular.length;
    } else {
      this.showMensagem = true;
    }
  }

  removerAutor(autor: Autor) {
   
    let indexRemove = this.autoresParaVicular.indexOf(autor);
    this.autoresParaVicular.splice(indexRemove, 1);
    this.autores.push(autor);
 
  }

  reloadData() {
    this.autorService.getAllAutores().subscribe(
      data => {
        this.autores = data.resposta;
        this.total = data.contador;
      },
      error => console.log("Erro ao carregar os Autores: " + error),
      () => console.log('Feito.')
    );
  }
}
