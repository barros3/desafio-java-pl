import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutorComponent } from './main-feature/autor/autor.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AutorListarComponent } from './main-feature/autor/autor-listar/autor-listar.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { NgxPaginationModule } from 'ngx-pagination'; 
import { ModalModule } from './shared/_modal';
import { ObraComponent } from './main-feature/obra/obra.component';
import { HomeComponent } from './main-feature/home/home.component';
import { MaskCpfCnpj } from './shared/util/maskCpfCnpj';

@NgModule({
  declarations: [
    AppComponent,
    AutorComponent,
    AutorListarComponent,
    ObraComponent,
    HomeComponent,
    MaskCpfCnpj
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    ModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
