
import { Directive, ElementRef, Input, HostListener, Output, EventEmitter } from '@angular/core';
import { ConverterCpfCnPjService } from '../../service/converter-cpf-cnpj.service';

declare var $: any;

@Directive({ selector: '[cpf-cnpj]' })
export class MaskCpfCnpj {

  @Output()
  onPressEnter: EventEmitter<any> = new EventEmitter();

  input: any;
  converter: ConverterCpfCnPjService = new ConverterCpfCnPjService();

  arrayNumber: any[] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  arrayFunction: any[] = [, "ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown"]

  constructor(private el: ElementRef) { }

  ngAfterViewInit() {
    this.input = $(this.el.nativeElement)['0'].name
  }

  @HostListener('keyup', ['$event']) onKeyUp(event: KeyboardEvent) {

  if (event.key == "Enter") {
      this.onPressEnter.emit();

  }else if (this.arrayFunction.indexOf(event.key) < 0) {
      let val = this.converter.convertToCpfCnpj($(this.el.nativeElement)['0'].value);
    $(this.el.nativeElement)['0'].value = val;
    }
  }
}
