import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutorService {

  private path = 'mood/autor';

  constructor(private http: HttpClient) { }

  getAllAutores(): Observable<any> {
    return this.http.get(`${this.path}` + "/list");
  }

  getAutorById(id: number): Observable<Object> {
    return this.http.get(`${this.path}/${id}`);
  }

  addAutor(obj: Object): Observable<Object> {
    return this.http.post(`${this.path}` + "/add", obj);
  }

  updateAutor(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.path}/${id}`, value);
  }

  deleteAutor(id: number): Observable<any> {
    return this.http.delete(`${this.path}/${id}`, { responseType: 'text' });
  }

  findAutorByCpf(obj: any): Observable<any> {
    return this.http.post(`${this.path}` + "/search", obj);
  }

}
