import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ObraService {

  private path = 'mood/obra';

  constructor(private http: HttpClient) { }

  getAllObras(): Observable<any> {
    return this.http.get(`${this.path}` + "/list");
  }

  getObraById(id: number): Observable<Object> {
    return this.http.get(`${this.path}/${id}`);
  }

  addObra(obj: Object): Observable<Object> {
    return this.http.post(`${this.path}` + "/add", obj);
  }

  updateObra(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.path}/${id}`, value);
  }

  deleteObra(id: number): Observable<any> {
    return this.http.delete(`${this.path}/${id}`, { responseType: 'text' });
  }

  findObraByCpf(obj: any): Observable<any> {
    return this.http.post(`${this.path}` + "/search", obj);
  }

}
