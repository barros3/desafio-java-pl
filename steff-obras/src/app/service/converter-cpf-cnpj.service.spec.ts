import { TestBed } from '@angular/core/testing';

import { ConverterCpfCnPjService } from './converter-cpf-cnpj.service';

describe('ConverterCpfCnPjService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConverterCpfCnPjService = TestBed.get(ConverterCpfCnPjService);
    expect(service).toBeTruthy();
  });
});
