
import { Obra } from './Obra';
import { Sexo } from '../shared/enun/sexo';

export class Autor {

  public Autor() {}

  id: number;
  nome: string;
  sexo: Sexo;
  email: string;
  dtNascimento: string;
  pai: string;
  cpf: number;
  obras: Obra[];
  obra: Obra;
}
