import { Autor } from './autor';

export class Obra {

  public Obra() {}

  id: any;
  nome: string;
  descricao: string;
  imagemTransiente: any;
  dtPublicacao: Date;
  dtExposicao: Date;
  autor: Autor[];

}
