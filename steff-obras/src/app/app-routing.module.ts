import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutorComponent } from './main-feature/autor/autor.component';
import { AutorListarComponent } from './main-feature/autor/autor-listar/autor-listar.component';
import { ObraComponent } from './main-feature/obra/obra.component';


const routes: Routes = [
  { path: 'mood', redirectTo: 'autor', pathMatch: 'full' },
  { path: 'mood/autor/add', component: AutorComponent },
  { path: 'mood/autor/list', component: AutorListarComponent },
  { path: 'mood/obra/add', component: ObraComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
