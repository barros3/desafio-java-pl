package com.lferreirab.engsoftpl.stefanini.mood.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.lferreirab.engsoftpl.stefanini.mood.enun.Sexo;

@Entity
@Table(name = "tb_autor")
public class Autor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 255)
	private String nome;
	
	@Enumerated(EnumType.ORDINAL)
	private Sexo sexo;
	
	@Column(unique = true, length = 60)
	private String email;
	
	@Column(name = "dtNascimento", nullable = false, length = 10)
	private Date dtNascimento;
	
	@Column(nullable = true)
	private String pais;

	@Column(nullable = true, unique = true)
	private String cpf;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
	name = "tb_autor_obra",
	joinColumns = {@JoinColumn(name = "id_pessoa") },
	inverseJoinColumns = {@JoinColumn(name = "id_obra") })
	private List<Obra> obras;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public List<Obra> getObras() {
		return obras;
	}

	public void setObras(List<Obra> obras) {
		this.obras = obras;
	}

	@Override
	public String toString() {
		return "Autor [id=" + id + ", nome=" + nome + ", sexo=" + sexo + ", email=" + email + ", dtNascimento="
				+ dtNascimento + ", pais=" + pais + ", cpf=" + cpf + ", obras=" + obras + "]";
	}
	
}
