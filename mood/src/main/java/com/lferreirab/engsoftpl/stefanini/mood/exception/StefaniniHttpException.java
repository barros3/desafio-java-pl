package com.lferreirab.engsoftpl.stefanini.mood.exception;

public class StefaniniHttpException extends Exception {

	private static final long serialVersionUID = 1L;

	public StefaniniHttpException(Object resourId) {
		super(resourId != null ? resourId.toString() : null);
	}
}