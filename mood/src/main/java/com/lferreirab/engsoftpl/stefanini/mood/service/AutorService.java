package com.lferreirab.engsoftpl.stefanini.mood.service;

import java.util.List;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import com.lferreirab.engsoftpl.stefanini.mood.model.Autor;

public interface AutorService {

	public List<Autor> listAutores();

	public void addAutor(Autor autor);

	public Autor getAutor(Long id) throws ResourceNotFoundException;

	public void dellAutor(Long id) throws ResourceNotFoundException;
	
}
