package com.lferreirab.engsoftpl.stefanini.mood.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lferreirab.engsoftpl.stefanini.mood.model.Autor;

@Repository("autorRepository")
public interface AutorRepository extends JpaRepository<Autor,Long> 	{

	Autor findByCpf(String cpf);
	
}
