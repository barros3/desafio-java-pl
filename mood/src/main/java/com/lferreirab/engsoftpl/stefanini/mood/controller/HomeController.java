package com.lferreirab.engsoftpl.stefanini.mood.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import com.lferreirab.engsoftpl.stefanini.mood.service.AuthorizationService;
import com.lferreirab.engsoftpl.stefanini.mood.service.DriveService;

@Controller
public class HomeController {

//	@GetMapping("/")
//	public String index() {
//		return "home/index";
//	}
	
	@Autowired
	AuthorizationService authorizationService;

	@Autowired
	DriveService driveService;

	@GetMapping("/")
	public String showHomePage() throws Exception {
		if (authorizationService.isUserAuthenticated()) {
			return "redirect:/home";
		} else {
			return "redirect:/login";
		}
	}

	@GetMapping("/login")
	public String goToLogin() {
		return "index.html";
	}
//
//	@GetMapping("/home")
//	public String goToHome() {
//		return "home.html";
//	}

	@GetMapping("/googlesignin")
	public void doGoogleSignIn(HttpServletResponse response) throws Exception {
		response.sendRedirect(authorizationService.authenticateUserViaGoogle());
	}

	@GetMapping("/oauth/callback")
	public String saveAuthorizationCode(HttpServletRequest request) throws Exception {
		String code = request.getParameter("code");
		if (code != null) {
			authorizationService.exchangeCodeForTokens(code);
			return "home.html";
		}
		return "index.html";
	}

	@GetMapping("/logout")
	public String logout(HttpServletRequest request) throws Exception {
		authorizationService.removeUserSession(request);
		return "redirect:/login";
	}

//	@PostMapping("/upload")
//	public String uploadFile(HttpServletRequest request, @ModelAttribute Autor uploadedFile) throws Exception {
//		MultipartFile multipartFile = uploadedFile.getMultipartFile();
//		driveService.uploadFile(multipartFile);
//		return "redirect:/home?status=success";
//	}
}
