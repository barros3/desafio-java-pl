package com.lferreirab.engsoftpl.stefanini.mood.service;

import java.util.List;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import com.lferreirab.engsoftpl.stefanini.mood.model.Obra;

public interface ObraService {

	public List<Obra> listObraes();

	public void addObra(Obra autor);

	public Obra getObra(Long id) throws ResourceNotFoundException;

	public void dellObra(Long id) throws ResourceNotFoundException;
	
}
