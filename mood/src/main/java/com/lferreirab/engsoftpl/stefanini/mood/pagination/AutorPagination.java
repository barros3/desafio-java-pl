package com.lferreirab.engsoftpl.stefanini.mood.pagination;

import java.util.List;

import com.lferreirab.engsoftpl.stefanini.mood.model.Autor;

public class AutorPagination {
	
	public AutorPagination() {}
	
	private int contador;
	
	private List<Autor> resposta;

	public int getContador() {
		return contador;
	}

	public void setContador(int contador) {
		this.contador = contador;
	}

	public List<Autor> getResposta() {
		return resposta;
	}

	public void setResposta(List<Autor> resposta) {
		this.resposta = resposta;
	}
	
}
