package com.lferreirab.engsoftpl.stefanini.mood.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.lferreirab.engsoftpl.stefanini.mood.constant.ApplicationConstant;
import com.lferreirab.engsoftpl.stefanini.mood.model.Obra;
import com.lferreirab.engsoftpl.stefanini.mood.pagination.ObraPagination;
import com.lferreirab.engsoftpl.stefanini.mood.repository.ObraRepository;
import com.lferreirab.engsoftpl.stefanini.mood.service.DriveService;
import com.lferreirab.engsoftpl.stefanini.mood.service.QuickStartService;

@RestController()
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/mood/obra")
public class ObraController {

	private static final Logger LOG = (Logger) LoggerFactory.getLogger(ObraController.class);

	@Autowired
	private ObraRepository obraRepository;
	
	@Autowired
	DriveService driveService;
	
	@Autowired
	QuickStartService quickStartService;
	
	@GetMapping(value = "/list")
	public ResponseEntity<ObraPagination> getObraes() {
		
		List<Obra> obras =  obraRepository.findAll();
		
		if(obras.isEmpty()) return new ResponseEntity<>(new ObraPagination(), HttpStatus.NOT_FOUND);
		else return new ResponseEntity<>(montarObjetoPaginacao(obras),  HttpStatus.OK);
	}
	
//	@PostMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<ObraPagination> getObra(@Valid @RequestBody Obra autor) {
//		
//		Obra autorDb = obraRepository.findByCpf(autor.getCpf());
//		
//		List<Obra> autores = new ArrayList<>(Arrays.asList(autorDb));
//		
//		if(autores.isEmpty())
//			return new ResponseEntity<>(new ObraPagination(), HttpStatus.NOT_FOUND);
//		else 
//			return new ResponseEntity<>(montarObjetoPaginacao(autores),  HttpStatus.OK);
//	}

	private ObraPagination montarObjetoPaginacao(List<Obra> obras) {
		
		ObraPagination obraPagination = new ObraPagination();
		obraPagination.setContador(obras.size());
		obraPagination.setResposta(obras);
		return obraPagination;
		
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Obra> getObraById(@PathVariable(name = "id") Long id) throws ResourceNotFoundException {
		
		Obra obra = obraRepository.findById(id).
				orElseThrow(() -> new ResourceNotFoundException("Obra não econtrado pelo id :: " + id));
		return ResponseEntity.ok().body(obra);
	}

	@SuppressWarnings("deprecation")
	@PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Obra> addObra(@Valid @RequestBody Obra obra, 
			@RequestParam(value = "imagemTransiente") MultipartFile multipartFile ) {
		
		if (obra.getDtExposicao() == null && obra.getDtPublicacao() == null) {
			return ResponseEntity.noContent().build();
		} else {
			
			if( obra.getDtExposicao() != null) obra.getDtExposicao().setDate(obra.getDtExposicao().getDate()+1);
			if( obra.getDtPublicacao() != null) obra.getDtPublicacao().setDate(obra.getDtPublicacao().getDate()+1);			
		}
		
		obraRepository.save(obra);
		
		return ResponseEntity.ok().body(obra);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Obra> updateObra(@PathVariable(value = "id") Long id, @Valid @RequestBody Obra obraView)
			throws ResourceNotFoundException {

		Obra obraBase = obraRepository.getOne(id);
		substituirDados(obraView, obraBase);
		final Obra obraAtualizado = obraRepository.save(obraBase);
		
		return ResponseEntity.ok(obraAtualizado);
	}

	private void substituirDados(Obra obraView, Obra obra) {
		
		obra.setNome(obraView.getNome());
		obra.setDtExposicao(obraView.getDtExposicao());
		obra.setDtPublicacao(obraView.getDtPublicacao());
		obra.setDescricao(obraView.getDescricao());
		obra.setImagem(obraView.getImagem());
		obra.setAutor(obraView.getAutor());
		
	}

	@DeleteMapping("/{id}")
	public Map<String, Boolean> deleteObra(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {

		Map<String, Boolean> response = null;
		
		Obra obra = obraRepository.getOne(id);		

		if (obra == null) {
			
			response = new HashMap<>();
			response.put("deleted", Boolean.FALSE);
			return response;
		}

		response = new HashMap<>();
		obraRepository.delete(obra);
		response.put("deleted", Boolean.TRUE);
		
		return response;
	}
	
	@PostMapping(value = "/upload/drive/v3/files?uploadType=multipart", produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveInCloud(@RequestParam(value = "imagemTransiente") MultipartFile multipartFile) {
		
	}
	
//	@PostMapping("/upload")
//	public String uploadFile(HttpServletRequest request, @ModelAttribute Obra uploadedFile) throws Exception {
//		MultipartFile multipartFile = uploadedFile.getImagemTransiente();
//		driveService.uploadFile(multipartFile);
//		return "redirect:/home?status=success";
//	}
	
	
	@PostMapping(value = "/upload", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String uploadFile(@ModelAttribute Obra obra) throws IOException, GeneralSecurityException {
		 
		// Build a new authorized API client service.
		
	    final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
	    Drive driveService = null;
	    
		try {
			driveService = new Drive.Builder(
					HTTP_TRANSPORT, ApplicationConstant.JSON_FACTORY, quickStartService.getCredentials(HTTP_TRANSPORT))
			        .setApplicationName(ApplicationConstant.APPLICATION_NAME)
			        .build();
		} catch (Exception e) {
			e.printStackTrace();
		}

	    File fileMetadata = new File();
	    fileMetadata.setName("42.jpg");
	    java.io.File filePath = new java.io.File("42.jpg");
	    FileContent mediaContent = new FileContent("image/jpeg", filePath);
	    
	    File file = driveService.files().create(fileMetadata, mediaContent).setFields("id").execute();
		
		
		return "redirect:/home?status=success";
	}
}
