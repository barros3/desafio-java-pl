package com.lferreirab.engsoftpl.stefanini.mood.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import com.lferreirab.engsoftpl.stefanini.mood.model.Autor;
import com.lferreirab.engsoftpl.stefanini.mood.repository.AutorRepository;
import com.lferreirab.engsoftpl.stefanini.mood.service.AutorService;

@Service
public class AutorServiceImpl implements AutorService {
	
	@Autowired
	private AutorRepository autorRepository;
	
	@Override
	public void addAutor(Autor autor) {
		autorRepository.save(autor);
	}

	@Override
	public List<Autor> listAutores() {
		return autorRepository.findAll();
	}

	@Override
	public Autor getAutor(Long id) throws ResourceNotFoundException {
		return autorRepository.getOne(id);
	}

	@Override
	public void dellAutor(Long id) throws ResourceNotFoundException {
		autorRepository.deleteById(id);
	}

}
