package com.lferreirab.engsoftpl.stefanini.mood.service;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.javanet.NetHttpTransport;

public interface QuickStartService {

    public Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws Exception;

}