package com.lferreirab.engsoftpl.stefanini.mood.service;

import org.springframework.web.multipart.MultipartFile;

public interface DriveService {
	
	public void uploadFile(MultipartFile multipartFile) throws Exception;
	
}
