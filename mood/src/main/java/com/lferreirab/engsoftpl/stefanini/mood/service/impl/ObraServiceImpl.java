package com.lferreirab.engsoftpl.stefanini.mood.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import com.lferreirab.engsoftpl.stefanini.mood.model.Obra;
import com.lferreirab.engsoftpl.stefanini.mood.repository.ObraRepository;
import com.lferreirab.engsoftpl.stefanini.mood.service.ObraService;

@Service
public class ObraServiceImpl implements ObraService {
	
	@Autowired
	private ObraRepository obraRepository;
	
	@Override
	public void addObra(Obra obra) {
		obraRepository.save(obra);
	}

	@Override
	public List<Obra> listObraes() {
		return obraRepository.findAll();
	}

	@Override
	public Obra getObra(Long id) throws ResourceNotFoundException {
		return obraRepository.getOne(id);
	}

	@Override
	public void dellObra(Long id) throws ResourceNotFoundException {
		obraRepository.deleteById(id);
	}

}
