package com.lferreirab.engsoftpl.stefanini.mood.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lferreirab.engsoftpl.stefanini.mood.model.Autor;
import com.lferreirab.engsoftpl.stefanini.mood.pagination.AutorPagination;
import com.lferreirab.engsoftpl.stefanini.mood.repository.AutorRepository;

@RestController()
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/mood/autor")
public class AutorController {

	private static final Logger LOG = (Logger) LoggerFactory.getLogger(AutorController.class);

	@Autowired
	private AutorRepository autorRepository;
	
	@GetMapping(value = "/list")
	public ResponseEntity<AutorPagination> getAutores() {
		
		List<Autor> autores =  autorRepository.findAll();
		
		if(autores.isEmpty())
			return new ResponseEntity<>(new AutorPagination(), HttpStatus.NOT_FOUND);
		else 
			return new ResponseEntity<>(montarObjetoPaginacao(autores),  HttpStatus.OK);
	}
	
	@PostMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AutorPagination> getAutor(@Valid @RequestBody Autor autor) {
		
		Autor autorDb = autorRepository.findByCpf(autor.getCpf());
		
		List<Autor> autores = new ArrayList<>(Arrays.asList(autorDb));
		
		if(autores.isEmpty())
			return new ResponseEntity<>(new AutorPagination(), HttpStatus.NOT_FOUND);
		else 
			return new ResponseEntity<>(montarObjetoPaginacao(autores),  HttpStatus.OK);
	}

	private AutorPagination montarObjetoPaginacao(List<Autor> autores) {
		AutorPagination autorPagination = new AutorPagination();
		autorPagination.setContador(autores.size());
		autorPagination.setResposta(autores);
		return autorPagination;
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Autor> getAutorById(@PathVariable(name = "id") Long id) throws ResourceNotFoundException {
		
		Autor autor = autorRepository.findById(id).
				orElseThrow(() -> new ResourceNotFoundException("Autor não econtrado pelo id :: " + id));
		return ResponseEntity.ok().body(autor);
	}

	@PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
	public Autor addAutor(@Valid @RequestBody Autor autor) {
		
		corrigirData(autor);
		
		return autorRepository.save(autor);
	}
	
	@SuppressWarnings("deprecation")
	void corrigirData(Autor autor) {
		autor.getDtNascimento().setDate(autor.getDtNascimento().getDate()+1);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Autor> updateAutor(@PathVariable(value = "id") Long id, @Valid @RequestBody Autor autorView)
			throws ResourceNotFoundException {

		Autor autorBase = autorRepository.getOne(id);
		substituirDados(autorView, autorBase);
		final Autor autorAtualizado = autorRepository.save(autorBase);
		
		return ResponseEntity.ok(autorAtualizado);
	}

	private void substituirDados(Autor autorView, Autor autor) {
		autor.setEmail(autorView.getEmail());
		autor.setDtNascimento(autorView.getDtNascimento());
		autor.setNome(autorView.getNome());
		autor.setPais(autorView.getPais());
		autor.setSexo(autorView.getSexo());
		autor.setObras(autorView.getObras());
	}

	@DeleteMapping("/{id}")
	public Map<String, Boolean> deleteAutor(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {

		Map<String, Boolean> response = null;
		
		Autor autor = autorRepository.getOne(id);		

		if (autor == null) {
			response = new HashMap<>();
			response.put("deleted", Boolean.FALSE);
			return response;
		}

		response = new HashMap<>();
		autorRepository.delete(autor);
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
