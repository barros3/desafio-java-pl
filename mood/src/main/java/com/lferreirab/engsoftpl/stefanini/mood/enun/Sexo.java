package com.lferreirab.engsoftpl.stefanini.mood.enun;

public enum Sexo {

	MASCULINO(0, "Masculino"), FEMININO(1, "Feminino"), OUTROS(2, "Outros");
	
	private final Integer chave;
	private final String valor;

	private Sexo(Integer chave, String valor) {
		this.chave = chave;
		this.valor = valor;
	}

	public Integer getChave() {
		return chave;
	}
	
	public String getValor() {
		return valor;
	}
}
