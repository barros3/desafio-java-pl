package com.lferreirab.engsoftpl.stefanini.mood.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lferreirab.engsoftpl.stefanini.mood.model.Obra;

@Repository("obraRepository")
public interface ObraRepository extends JpaRepository<Obra,Long> 	{
	
}
