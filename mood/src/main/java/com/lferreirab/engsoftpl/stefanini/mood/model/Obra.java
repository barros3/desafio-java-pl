package com.lferreirab.engsoftpl.stefanini.mood.model;

import java.sql.Blob;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name = "tb_obra")
public class Obra {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 255)	
	private String nome;
	
	@Column(nullable = false, length = 240)	
	private String descricao;
	
	@Transient
	private MultipartFile imagemTransiente;	
	
	@Column(nullable = false)
	private Blob imagem;
	
	@Column(name = "dtPublicacao", length = 10)	
	private Date dtPublicacao;
	
	@Column(name = "dtExposicao",length = 10)	
	private Date dtExposicao;
	
	@NotNull
	@ManyToMany(mappedBy="obras")
	private List<Autor> autor;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDtPublicacao() {
		return dtPublicacao;
	}

	public void setDtPublicacao(Date dtPublicacao) {
		this.dtPublicacao = dtPublicacao;
	}

	public Date getDtExposicao() {
		return dtExposicao;
	}

	public void setDtExposicao(Date dtExposicao) {
		this.dtExposicao = dtExposicao;
	}

	public List<Autor> getAutor() {
		return autor;
	}

	public void setAutor(List<Autor> autor) {
		this.autor = autor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Blob getImagem() {
		return imagem;
	}

	public void setImagem(Blob imagem) {
		this.imagem = imagem;
	}

	public MultipartFile getImagemTransiente() {
		return imagemTransiente;
	}

	public void setImagemTransiente(MultipartFile imagemTransiente) {
		this.imagemTransiente = imagemTransiente;
	}

	@Override
	public String toString() {
		return "Obra [id=" + id + ", nome=" + nome + ", descricao=" + descricao + ", imagem=" + imagem
				+ ", dtPublicacao=" + dtPublicacao + ", dtExposicao=" + dtExposicao + ", autor=" + autor + "]";
	}

}
