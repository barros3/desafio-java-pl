package com.lferreirab.engsoftpl.stefanini.mood.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
@ComponentScan("com.lferreirab.engsoftpl.stefanini.mood")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

//	@Autowired
//	DriverManagerDataSource dataSource;
//
//	@Bean
//	@Override
//	public AuthenticationManager authenticationManagerBean() throws Exception {
//		return super.authenticationManagerBean();
//	}

//	@Bean
//	public PasswordEncoder passwordEncoder() {
//		return new BCryptPasswordEncoder();
//	}

//	@Autowired
//	public void configure(AuthenticationManagerBuilder auth) throws Exception {
//		
//		auth.inMemoryAuthentication()
//	    .withUser("spring")
//	    .password("secret")
//	    .roles("USER");
//
//		auth.jdbcAuthentication().dataSource(dataSource);
//	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/bootstrap/**", "/resources/**",
				"/startbootstrap-business-frontpage/**", "/vendor/**", "/img/**");
	}

	@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
		return new HttpSessionEventPublisher();
	}

//	@Bean
//	public SessionRegistry sessionRegistry() {
//		SessionRegistry sessionRegistry = new SessionRegistryImpl();
//		return sessionRegistry;
//	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/**").permitAll()
//			.and().formLogin().loginProcessingUrl("/").loginPage("/")
//			.usernameParameter("username").passwordParameter("password").defaultSuccessUrl("/successo", true)
//			.permitAll().failureUrl("/erro").permitAll().and().logout()
//			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))

			.and().exceptionHandling().accessDeniedPage("/403")

			.and().csrf().disable();
	}

}
