package com.lferreirab.engsoftpl.stefanini.mood.pagination;

import java.util.List;

import com.lferreirab.engsoftpl.stefanini.mood.model.Obra;

public class ObraPagination {
	
	public ObraPagination() {}
	
	private int contador;
	
	private List<Obra> resposta;

	public int getContador() {
		return contador;
	}

	public void setContador(int contador) {
		this.contador = contador;
	}

	public List<Obra> getResposta() {
		return resposta;
	}

	public void setResposta(List<Obra> resposta) {
		this.resposta = resposta;
	}
	
}
