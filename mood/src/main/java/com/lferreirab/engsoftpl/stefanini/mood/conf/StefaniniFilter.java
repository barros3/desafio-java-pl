package com.lferreirab.engsoftpl.stefanini.mood.conf;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class StefaniniFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)  throws IOException, ServletException {
		
		HttpServletResponse servResponse = (HttpServletResponse) response;

		servResponse.setHeader("Access-Control-Allow-Origin", "*");
		servResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
		servResponse.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
		servResponse.setHeader("Access-Control-Max-Age", "3600");
		servResponse.setHeader("Access-Control-Allow-Headers", "content-type, Accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

		chain.doFilter(request, response);
		
	}
	
	public void init(FilterConfig filterConfig) {}
	 
    public void destroy() {}
		
}
